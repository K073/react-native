import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default class App extends React.Component {
  state = {
    text: ''
  };

  addValue = (value) => {
    this.setState({text: this.state.text + value})
  };

  deleteLastSymbol = () => {
    const arrFromText = [...this.state.text];
    arrFromText.pop();
    const text = arrFromText.join('');
    this.setState({text})
  };

  render() {
    return (
      <View>
        <View style={styles.container}>
          <Text
            style={{height: 40, borderColor: 'gray', fontSize: 20, width: '100%', textAlign: 'right'}}
          >{this.state.text}</Text>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(1)}>
            <Text>1</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(2)}>
            <Text>2</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(3)}>
            <Text>3</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(4)}>
            <Text>4</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(5)}>
            <Text>5</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(6)}>
            <Text>6</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(7)}>
            <Text>7</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(8)}>
            <Text>8</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(9)}>
            <Text>9</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue(0)}>
            <Text>0</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue('+')}>
            <Text>+</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue('-')}>
            <Text>-</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue('/')}>
            <Text>/</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.addValue('*')}>
            <Text>*</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={this.deleteLastSymbol}>
            <Text>{'<'}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity style={styles.button}
                            onPress={() => this.setState({text: ''})}>
            <Text>CE</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
                            onPress={() => {
                              let result = '';
                              try {
                                result = String(eval(this.state.text));
                              } catch (e) {
                                alert('error');
                              }
                              this.setState({text: result})
                            }}>
            <Text>=</Text>
          </TouchableOpacity>
        </View>
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    backgroundColor: '#fff',
  },
  btnContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button: {
    width: '19%',
    height: 40,
    backgroundColor: 'skyblue',
    alignItems: 'center',
    margin: 1,
  }
});
